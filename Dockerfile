# base image
FROM node:latest

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY app/package.json app/package-lock.json /usr/src/app/
RUN npm install --no-progress --ignore-optional

# Bundle app source
COPY app /usr/src/app

# Generate zip sources to share
# comment for CI, uncomment to generate sources
# RUN npm run zip

# Expose port
EXPOSE 8080

# Default command to run
CMD ["npm", "start"]
