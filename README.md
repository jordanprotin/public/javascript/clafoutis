> Auteur : Jordan Protin | jordan.protin@yahoo.com

# Clafoutis

Il s'agit d'un projet javascript permettant la gestion du consentement des **cookies** par les internautes sur un site web.

> Cet outil se calque sur le modèle proposé par [tarteaucitron](https://tarteaucitron.io/fr/) mais permet en plus une compatibilité avec [Google Tag Manager](https://tagmanager.google.com/#/home).

## Prérequis

- [GIT](https://git-scm.com/)
- [NodeJs](https://nodejs.org/en/)
- [NPM](https://www.npmjs.com/get-npm)

## Installation

``` bash
$ git clone git@gitlab.com:jordanprotin/open/js/clafoutis.git
```

## Documentation

Une documentation technique du projet est rapidement accessible en exécutant ces lignes de commandes suivantes :

``` bash
$ cd clafoutis/app
$ npm run docs
```

> L'interface de cette documentation sera alors accessible sur [http://localhost:4040/](http://localhost:4040/).

> :exclamation: Une documentation fonctionnelle du projet est à retrouver dans le fichier `app/src/index.html`