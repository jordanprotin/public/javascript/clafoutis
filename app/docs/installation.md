> Auteur : Jordan Protin | jordan.protin@yahoo.com

# Documentation du projet Clafoutis : section installation

# Sommaire

- [Prérequis](#prérequis)
- [Installation](#installation)

# Prérequis

- [Node.js & Npm](https://www.npmjs.com/get-npm)

# Installation

> :exclamation: La configuration Docker de ce projet est seulement utile pour un déploiement sur [Rancher](https://rancher.com/).
> En mode dév, nous préferons ne pas l'utiliser pour plus de simplicité.

## En mode dév

#### Installation des dépendances

``` bash
$ cd clafoutis/app
$ npm install
```

#### Lancement

``` bash
$ cd clafoutis/app
$ npm run start
```

> :+1: Vous pouvez désormais accéder à l'interface sur [http://localhost:8080/](http://localhost:8080/). 

> :exclamation: Veuillez noter que chaque modification refraîchira automatiquement l'UI à la manière d'un hot-reloading.

## En mode production

#### Construction des sources minifiées

``` bash
$ cd clafoutis/app
$ npm run build     # construit les sources minifiées sans générer une archive zip dans clafoutis/app/dist
$ npm run zip       # génère une archive zip dans clafoutis/app/shared dans le but de la partager pour intégration sur un site web
```

> :exclamation: Veuillez noter que ces lignes de commandes sont aussi exécutées pour l'intégration continue via le fichier `.gitlab-ci.yml`. 
> Nul besoin donc de les utiliser en mode dév.