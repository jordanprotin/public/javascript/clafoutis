> Auteur : Jordan Protin | jordan.protin@yahoo.com

## Documentation du projet Clafoutis : section Rancher

### Documentation existante

Une interface faisant office de documentation fonctionelle a été créée pour ce projet. Il s'agit du fichier `app/src/index.html`.

### Déploiement de l'interface sur Rancher

Il peut être nécessaire de devoir recréer la **stack** sur Rancher.
Pour ce faire, lorsque l'on vous demandera la configuration du `docker-composer.yml`, voici ce que vous devez copier/coller :

``` yaml
version: '2'

services:

  web:
    image: registry.gitlab.com/jordanprotin/internes/statiques/js/clafoutis
    environment:
      NODE_ENV: production
    user: node
    labels:
      traefik.frontend.rule: Host:clafoutis.preview.byjordanprotin.com
      traefik.port: '8080'
      traefik.enable: 'true'
      io.rancher.container.pull_image: always
```