## Project structure

```bash
├── app                                       # The app directory
|   ├── docs                                  # Documentation files
|   ├── node_modules                          # npm package manager installs packages locally into the project
|   ├── shared                                # Contains the finale zip to share with developers
|   ├── src                                   # Sources directory
|   |   ├── js                                # JavaScript files
|   |   |   ├── modules                       # The app logic is divided into many modules
|   |   |   |   ├── _config.js                # Init configuration
|   |   |   |   ├── _cookie.js                # Specially for cookies management
|   |   |   |   ├── _interface.js             # Specially for UI management
|   |   |   |   ├── _services.js              # List of the known services
|   |   |   |   └── _settings.js              # Settings configuration (can be overrides)
|   |   |   └── clafoutis.js                  # The app's entrypoint (which calls the other modules)
|   |   ├── json                              # External sources
|   |   |   └── GTM-KSJG7MH_workspace13.json  # An example of a GTM container which implements Clafoutis
|   |   ├── lang                              # Languages directory
|   |   |   ├── en.json                       # English translations
|   |   |   └── fr.json                       # French translations
|   |   ├── scss                              # The app style is divided into many SCSS files
|   |   |   ├── _buttons.scss                 # Specially for buttons style
|   |   |   ├── _clafoutis.scss               # The main style
|   |   |   ├── _cookies-bar.scss             # Specially for the cookies banner
|   |   |   ├── _variables.scss               # Contains many variables that can reuse in the other SCSS files
|   |   |   └── app.scss                      # The main SCSS file which import the other SCSS files and will be minified
|   |   └── index.html                        # The UI guidelines (optional for this tool)
|   ├── .babelrc                              # Local configuration for code
|   ├── .gitignore                            # Ignore all files which don't have to commit in GIT
|   ├── package-lock.json                     # Save all packages version
|   ├── package.json                          # List of all packages/dependencies to install
|   ├── postcss.config.js                     # Loader for webpack to process CSS
|   └── webpack.config.js                     # Defines all instructions to execute in order to deploy the app sources
├── .dockerignore                             # Ignore all files which should not be taken into account by Docker
├── .gitlab-ci.yml                            # For GitLab CI
├── .sass-lint.yml                            # Linter for sass conventions
├── Dockerfile                                # Build the Docker image of this project
├── README.md                                 # Basic readme file
├── docker-compose.yml                        # Management of the docker containers needed
└── sonar-project.properties                  # Controls the quality of the code by using sonarQube
``` 