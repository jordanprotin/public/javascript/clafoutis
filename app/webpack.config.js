const path = require("path");
const I18nPlugin = require("i18n-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const AggressiveMergingPlugin = require('webpack/lib/optimize/AggressiveMergingPlugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const relativePath = path.resolve(__dirname, './');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const postcssLoader = {
  loader: 'postcss-loader', // postcss loader so we can use autoprefixer
  options: {
    config: {
      path: './postcss.config.js'
    }
  }
};

// defines all translations as possible
const languages = {
	en: require("./src/lang/en.json"),
	fr: require("./src/lang/fr.json")
};

// build a clafoutis.[lang].js file for each translation
module.exports = Object.keys(languages).map(function(language) {
	return {
		name: language,
		mode: 'production',
		entry: {
			'clafoutis': ['./src/js/clafoutis']
		},
	  output: {
			path: relativePath + '/dist',
			filename: '[name].' + language + '.js',
	    libraryTarget: "var", // export itself to a global var
	    library: '[name]' // name of the global var to manipulate it from HTML file
		},
		plugins: [
			new I18nPlugin(
				languages[language]
			),
			new ExtractTextPlugin({
		    filename: 'clafoutis.css',
		    allChunks: true,
		  }),
		  new HtmlWebpackPlugin({
	    	inject: false, // no inject resources directly in HTML file
	    	template: 'src/index.html'
	  	}),
	  	new UglifyJsPlugin(), // minify everything
    	new AggressiveMergingPlugin(), // merge chunks
	  	new CleanWebpackPlugin(['dist'], {
	    	root: relativePath
	    }),
	    new CopyWebpackPlugin([
    		{ from: './src/json/GTM-KSJG7MH_workspace13.json', to: '' },
    		{ from: './shared/clafoutis.zip', to: '' }
  		]),
		],
		module: {
			rules: [
	      {
	        test: /\.js?$/,
	        exclude: /node_modules/,
	        use: [
	        	{
	          	loader: 'babel-loader',
	          	options: {
	            	presets: ['@babel/preset-env']
	          	}
	          }
	        ]
	      },
	      {
	        test: /\.s?css/,
	        use: ExtractTextPlugin.extract({
	          fallback: 'style-loader',
	          use: [
	            {
	              loader: 'css-loader' // translates CSS into CommonJS modules
	            },
	            postcssLoader,
	            {
	              loader: 'sass-loader' // compiles SASS to CSS
	            }
	          ]
	        })
	      }
	    ]
		},
		devServer: {
			contentBase: relativePath + '/dist',
	    compress: true,
	    disableHostCheck: true // necessary for Rancher
	  }
	};
});