'use strict';

import settings from './_settings.js';
import  { 
  addClassToId,
  removeClassToId,
  displayServicesInToolBox,
  genericDisplayMessage 
}  from './_interface.js';

/**
 * Build cookie value & 
 * display services found in the tool box
 * 
 * @param  {Array}  wishedServices  -  The wished services list to install
 * 
 * @return {none}
 */
export function prepareCookieAndDisplayServices(wishedServices) {
  var allServices = [],
      servicesCodeInCookie = [],
      highPrivacy = settings.highPrivacy,
      currentCookieValue = readCookie();

  if (wishedServices.length > 0) {
    allServices = getDefinitiveServicesList(wishedServices);
    settings.allServices = allServices;

    // display services found in the tool box
    for (let [key, value] of Object.entries(allServices)) {
      // adding an id on each service 
      // to uniquely identify them
      value.id = key; 
      displayServicesInToolBox(value);
    }

    // -----------------------------------------------------------------
    // -- purge installed cookies for all services which are disabled --
    // -----------------------------------------------------------------
    
    // there are several services separated by '|'
    if (currentCookieValue.includes('|')) {
      servicesCodeInCookie = currentCookieValue.split('|');

      for (let [key, value] of Object.entries(allServices)) {
        if (!servicesCodeInCookie.includes(value.code) && value.cookies !== undefined) {
          purgeInstalledCookies(value.cookies);
        }
      }

    } else { // there is only one or no service

      for (let [key, value] of Object.entries(allServices)) {
        if (currentCookieValue !== value.code && value.cookies !== undefined) {
          purgeInstalledCookies(value.cookies);
        }
      }

    }

    buildAndSaveCookie(highPrivacy);
  } else {
    genericDisplayMessage(settings.lang.noServices, true);
    saveCookie('');
  }
}

/**
 * Build cookie value and save it 
 * 
 * @param  {boolean}  highPrivacy  -  true|false
 * 
 * @return {none}
 */
function buildAndSaveCookie(highPrivacy) {
  var DOMContentLoadedFirstTime = localStorage.getItem("DOMContentLoadedFirstTime"),
      alreadyAsk = localStorage.getItem("isOperationAsked"),
      cookieValue = '';

  if (DOMContentLoadedFirstTime == null) { // first time loaded!
    localStorage.setItem("DOMContentLoadedFirstTime", true);      
  } else {
    if (readCookie()) {
      cookieValue = readCookie();
    }
  }

  // delete fhe first '|' encountred 
  if (cookieValue !== '' && cookieValue.includes('|')) {
    cookieValue = cookieValue.substring(0); 
  } 

  if (alreadyAsk === "true") {
    saveCookie(cookieValue); 
    changeBackgroundOfAllAllowedServicesButton(cookieValue);
  } else { // default is shortcutAllowAllServices()
    highPrivacy ? shortcutDenyAllServices() : shortcutAllowAllServices();  
  }
}

/**
 * Merge known services & unknown services 
 * to get a definitive services list
 * 
 * @param  {Array}  wishedServices  -  The services which are wished
 * 
 * @return {none}
 */
function getDefinitiveServicesList(wishedServices) {
  var knownServices = [],
      unknownServices = [];

  for (let service of wishedServices) {
    var serviceCode = service.code;

    // get known services
    if (serviceCode) {
      if (settings.knownServices[serviceCode]) {
        // overrides translations here
        knownServices.push({
          'code': service.code ? service.code : settings.knownServices[serviceCode].code,
          'title': service.title ? service.title : settings.knownServices[serviceCode].title,
          'category': service.category ? service.category : settings.knownServices[serviceCode].category,
          'description': service.description ? service.description : settings.knownServices[serviceCode].description,
          'uri': service.uri ? service.uri : settings.knownServices[serviceCode].uri,
          'cookies': service.cookies ? service.cookies : settings.knownServices[serviceCode].cookies
        }); 
      } 
      // get unknown services 
      // title & category are the two required fields for this case
      // otherwise, the given tag don't respect config convention 
      // and will be ignored
      else if (service.title && service.category) { 
        unknownServices.push(service);
      } 
    }
  }

  return knownServices.concat(unknownServices);
}


/**
 * Save the given values in cookie value
 * 
 * @param  {string}  values  -  Service(s) code(s) separated by a '|'
 * 
 * @return {none}
 */
function saveCookie(values) {
  var d = new Date(),
      time = d.getTime(),
      expireTime = time + 31536000000,
      value = 'clafoutis=' + values;

  d.setTime(expireTime);

  document.cookie = value + '; expires=' + d.toGMTString() + '; path=/;';
}

/**
 * Read the cookie value which called « clafoutis »
 * 
 * @return {string}  -  The cooke value found
 */
function readCookie() {
  var nameEQ = "clafoutis=",
      ca = document.cookie.split(';'),
      i,
      c;

  for (i = 0; i < ca.length; i += 1) {
    c = ca[i];

    while (c.charAt(0) === ' ') {
      c = c.substring(1, c.length);
    }

    if (c.indexOf(nameEQ) === 0) {
      return c.substring(nameEQ.length, c.length);
    }
  }

  return '';
}

/**
 * see readCookie() comments
 */
export function genericReadCookie() {
  return readCookie();
}

/**
 * Deny a specific service
 * 
 * @param  {string}  serviceCode  -  The given service code (Ex: analytics)
 * 
 * @return {none} 
 */
export function denyService(serviceCode) {
  var allServices = settings.allServices,
      currentCookieValue = readCookie(),
      cookieValueInProgress = '',
      cookieValueToSave = '';

  localStorage.setItem("isOperationAsked", true);

  // delete the service code from cookie value
  if (currentCookieValue.includes('|' + serviceCode)) {
    cookieValueInProgress = currentCookieValue.replace('|' + serviceCode, '');
  } else {
    cookieValueInProgress = currentCookieValue.replace(serviceCode, '');
  }

  cookieValueToSave = cookieValueInProgress;

  // checks if necessary to delete the first '|' encountred
  if (cookieValueInProgress.includes('|')) {
    if (cookieValueInProgress.split('|')[0] == '') {
      cookieValueToSave = cookieValueInProgress.substring(1);
    } else {
      cookieValueToSave = cookieValueInProgress.substring(0);
    }  
  }

  // if it was the last service,
  // so update the cookie to empty
  if (cookieValueToSave) {
    saveCookie(cookieValueToSave); 
  } else {
    saveCookie('');
    addClassToId(settings.cssLabel + 'AllDenied', 'active');
  }

  removeClassToId(serviceCode + 'Allowed', 'active');
  addClassToId(serviceCode + 'Denied', 'active');
  removeClassToId(settings.cssLabel + 'AllAllowed', 'active');
}

/**
 * Allow a specific service
 * 
 * @param  {string}  serviceCode  -  The given service code (Ex: analytics)
 * 
 * @return {none} 
 */
export function allowService(serviceCode) {
  var currentCookieValue = readCookie(),
      cookieValueToSave = currentCookieValue;

  localStorage.setItem("isOperationAsked", true);

  // the cookie is not empty,
  // so you must complete the cookie value 
  // with the given service code
  if (currentCookieValue && !currentCookieValue.includes(serviceCode)) {
    if (currentCookieValue.length >= 1) {
      cookieValueToSave = currentCookieValue + '|' + serviceCode;
    } else {
      cookieValueToSave = currentCookieValue + serviceCode;
    }
  } else { // create a new cookie value with the given service code
    cookieValueToSave = serviceCode;
  }

  saveCookie(cookieValueToSave);

  removeClassToId(serviceCode + 'Denied', 'active');
  addClassToId(serviceCode + 'Allowed', 'active');
  removeClassToId(settings.cssLabel + 'AllDenied', 'active');

  changeBackgroundOfAllAllowedServicesButton(cookieValueToSave);
}

/**
 * Extract cookie value with the given services list
 * 
 * @param  {Object}  allServices  -  The given services list
 * 
 * @return {string}  The cookie value built
 */
function getCookieValueByServicesList(allServices) {
  var cookieValue = '';

  for (let [key, value] of Object.entries(allServices)) {
    if (key >= 0) {
      cookieValue += '|' + value.code;
    }
  }
  
  if (cookieValue.includes('|')) {
    cookieValue = cookieValue.substring(1);
  }

  return cookieValue;
}

/**
 * Allow all services
 * 
 * @return {none}
 */
export function allowAllServices() {
  var allServices = settings.allServices,
      currentCookieValue = getCookieValueByServicesList(allServices);

  localStorage.setItem("isOperationAsked", true);
  saveCookie(currentCookieValue);

  addClassToId(settings.cssLabel + 'AllAllowed', 'active');
  removeClassToId(settings.cssLabel + 'AllDenied', 'active');
  // change background color of all service ask buttons
  for (let service of allServices) {
    addClassToId(service.code + 'Allowed', 'active');
    removeClassToId(service.code + 'Denied', 'active');
  }
}

/**
 * see allowAllServices() comments
 */
function shortcutAllowAllServices() {
  allowAllServices();
  localStorage.setItem("isOperationAsked", false);
}

/**
 * Deny all services
 * 
 * @return {none}
 */
export function denyAllServices() {
  var allServices = settings.allServices;

  localStorage.setItem("isOperationAsked", true);

  saveCookie('');

  removeClassToId(settings.cssLabel + 'AllAllowed', 'active');
  addClassToId(settings.cssLabel + 'AllDenied', 'active');

  // change background color of all service ask buttons
  for (let service of allServices) {
    removeClassToId(service.code + 'Allowed', 'active');
    addClassToId(service.code + 'Denied', 'active');
  } 
}

/**
 * see denyAllServices() comments
 */
function shortcutDenyAllServices() {
  denyAllServices();
  localStorage.setItem("isOperationAsked", false);
}

/**
 * Delete all cookies which are associated 
 * to a specific service to delete
 * 
 * @param  {array}  cookies  -  The cookies list (Ex: ['_ga', '_gat'])
 * 
 * @return {none}
 */
function purgeInstalledCookies(cookies) {
  var i;

  for (i = 0; i < cookies.length; i += 1) {
    document.cookie = cookies[i] + '=; expires=Thu, 01 Jan 2000 00:00:00 GMT; path=/;';
  }
}

/**
 * Determines if necessary to change background-color to green color 
 * of « Autoriser tout » button
 *
 * @param  {string}  definitiveCookieValue  -  Definitive cookie value
 * 
 * @return {none} 
 */
function changeBackgroundOfAllAllowedServicesButton(definitiveCookieValue) {
  var allServices = settings.allServices,
      actualCookieValue = '',
      isAllActivated = false;

  actualCookieValue = getCookieValueByServicesList(allServices);
  
  if (actualCookieValue.length === definitiveCookieValue.length) {
    isAllActivated = true;
  }

  isAllActivated ? addClassToId(settings.cssLabel + 'AllAllowed', 'active') : null;
}

/**
 * Add a cookie for a specific service within his cookies list
 * 
 * @param  {Object}  wishedCookies  -  The wished cookies list to add
 * @param  {Array}  wishedServices  -  The wished services list to install
 * @param  {string}  UA  -  The Google Analytics UA
 * 
 * @return {none}
 */
export function addCookieForSpecificService(wishedCookies, wishedServices, UA) {
  if (null === UA && null !== wishedCookies) {
    Object.entries(wishedCookies).forEach(([serviceCode, cookiesList]) => {
      pushOrSpliceArrayCookies(wishedServices, null, serviceCode, cookiesList);
    });
  } else { 
    pushOrSpliceArrayCookies(wishedServices, UA, null, null);
  }
}

/**
 * Delete a cookie from the specific service's cookies list
 * 
 * @param  {Object}  wishedCookies  -  The wished cookies list to delete
 * @param  {Array}  wishedServices  -  The wished services list to install
 * 
 * @return {none}
 */
export function deleteCookieForSpecificService(wishedCookies, wishedServices) {
  Object.entries(wishedCookies).forEach(([serviceCode, cookiesList]) => {
    pushOrSpliceArrayCookies(wishedServices, null, null, cookiesList);
  });
}

/**
 * Push or splice given cookies list for the associated service
 *
 * @param  {Array}  wishedServices  -  The wished services list to install
 * @param  {string}  UA  -  The Google Analytics UA
 * @param  {string}  serviceCode  -  The service code if UA == null
 * @param  {Array}  cookiesList  -  The cookies list to add 
 * 
 * @return {none} 
 */
function pushOrSpliceArrayCookies(wishedServices, UA, serviceCode, cookiesList) {
  var allServices = getDefinitiveServicesList(wishedServices),
      serviceCookies = [],
      index;

  for (let [serviceKey, serviceValue] of Object.entries(allServices)) {
    serviceCookies = serviceValue.cookies;

    // Case 1 : specific traitment for Google Analytics UA cookie
    if (null !== UA && serviceValue.code == 'analytics') {
      serviceCookies.push('_gat_UA-' + UA);
    } else if (null !== cookiesList) {
      for (let cookie of cookiesList) {
        // Case 2 : add cookie by his name within the service's cookies list
        if (serviceCode !== null && serviceCode === serviceValue.code) {
          serviceCookies.push(cookie);
        } else { // Case 3 : delete cookie by his name from service's cookies list
          index = serviceCookies.indexOf(cookie);

          if (index != -1) {
            serviceCookies.splice(index, 1);
          }
        }
      }
    }
  } 

  // avoid duplicates values for cookies entries
  for (let [key, value] of Object.entries(allServices)) {
    value.cookies = removeDuplicates(value.cookies);
  }
}


/**
 * Remove duplicates from array using for loop
 * 
 * @param  {Array}  arr  -  The given array
 * @return {Array}  Array rebuilt after remove duplicates
 */
function removeDuplicates(arr){
  let unique_array = [];

  for(let i = 0; i < arr.length; i++){
    if(unique_array.indexOf(arr[i]) == -1){
      unique_array.push(arr[i]);
    }
  }
  
  return unique_array;
}