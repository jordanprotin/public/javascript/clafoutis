'use strict';

import  { prepareCookieAndDisplayServices }  from './_cookie.js';
import  { buildToolBox, displayCookieBanner }  from './_interface.js';
import settings from './_settings.js';
import knownServices from './_services.js';

/**
 * Start of configuration -
 * auto-execution of this script
 * 
 * @param  {Object}  params  -  highPrivacy and/or cssLabel
 * 
 * @return {none}
 */
export function init(params) {
	var highPrivacy         = params.highPrivacy,
	    cssLabel            = params.cssLabel,
      cookiesBanner       = params.cookiesBanner,
      lang                = params.lang,
	    agreeCookiesConsent = localStorage.getItem("agreeCookiesConsent");

  highPrivacy   ? settings.highPrivacy = highPrivacy : null;
  cssLabel      ? settings.cssLabel    = cssLabel    : null;
  lang          ? langOverrides(lang)                : null;

  if (cookiesBanner) {
    if (cookiesBanner.active !== undefined && 
          cookiesBanner.active === false) {
      settings.cookiesBanner.active = cookiesBanner.active;
    }

    if (cookiesBanner.appendChildId !== undefined && 
          cookiesBanner.appendChildId !== 'body') {
      settings.cookiesBanner.appendChildId = cookiesBanner.appendChildId;
    }
  }

  settings.knownServices = knownServices;

  buildToolBox();

  if ((agreeCookiesConsent === "false" 
        || agreeCookiesConsent === null) 
  			   && settings.cookiesBanner.active) {
  	displayCookieBanner();
  }
}

/**
 * End of configuration - Google Tag Manager (GTM) is well triggered
 * we can set the services
 * 
 * @param  {Object}  wishedServices  -  The wished services list
 * 
 * @return {none}
 */
export function run(wishedServices) {
  prepareCookieAndDisplayServices(wishedServices);
}

/**
 * Language translations overrides
 * 
 * @param  {Object} lang - The given lang instance
 * @return {none}
 */
function langOverrides(lang) {
  lang.mainTitle                   ? settings.lang.mainTitle                   = lang.mainTitle                   : null;
  lang.disclaimer                  ? settings.lang.disclaimer                  = lang.disclaimer                  : null;
  lang.askTitle                    ? settings.lang.askTitle                    = lang.askTitle                    : null;
  lang.allow                       ? settings.lang.allow                       = lang.allow                       : null;
  lang.allowAll                    ? settings.lang.allowAll                    = lang.allowAll                    : null;
  lang.deny                        ? settings.lang.deny                        = lang.deny                        : null;
  lang.denyAll                     ? settings.lang.denyAll                     = lang.denyAll                     : null;
  lang.noServices                  ? settings.lang.noServices                  = lang.noServices                  : null;
  lang.useAdblocker                ? settings.lang.useAdblocker                = lang.useAdblocker                : null;
  lang.cookieBannerMessage         ? settings.lang.cookieBannerMessage         = lang.cookieBannerMessage         : null;
  lang.cookieBannerAgreeButton     ? settings.lang.cookieBannerAgreeButton     = lang.cookieBannerAgreeButton     : null;
  lang.cookieBannerClafoutisButton ? settings.lang.cookieBannerClafoutisButton = lang.cookieBannerClafoutisButton : null;
  lang.msgTitle                    ? settings.lang.msgTitle                    = lang.msgTitle                    : null;
}