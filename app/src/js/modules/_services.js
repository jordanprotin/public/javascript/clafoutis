var _services = {
  // google analytics
  'analytics' : {
    'code': 'analytics',
    'category': 'Mesure d\'audience',
    'title': 'Google Analytics (universal)',
    'description': 'Les services de mesure d\'audience permettent de générer des statistiques de fréquentation utiles à l\'amélioration du site.',
    'uri': 'https://support.google.com/analytics/answer/6004245',
    'cookies': ['_ga', '_gat', '_gid', '__utma', '__utmb', '__utmc', '__utmt', '__utmz']
  },

  // googleadwordsconversion
  'googleadwordsconversion' : {
    'code': 'googleadwordsconversion',
    'category': 'Régies publicitaires',
    'title': 'Google Adwords (conversion)',
    'description': 'Les régies publicitaires permettent de générer des revenus en commercialisant les espaces publicitaires du site.',
    'uri': 'https://www.google.com/settings/ads',
    'cookies': []
  },

  // googleadwordsremarketing
  'googleadwordsremarketing' : {
    'code': 'googleadwordsremarketing',
    'category': 'Régies publicitaires',
    'title': 'Google Adwords (remarketing)',
    'description': 'Les régies publicitaires permettent de générer des revenus en commercialisant les espaces publicitaires du site.',
    'uri': 'https://www.google.com/settings/ads',
    'cookies': []
  }

  // add your services here
}

module.exports = _services