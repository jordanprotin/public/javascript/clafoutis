'use strict';

import settings from './_settings.js';
import  { genericReadCookie }  from './_cookie.js';

/**
 * Build the tool box without services list
 * 
 * @return {none}
 */
export function buildToolBox() {
  // The load event is triggered when 
  // all resources are entirely loaded.
  window.addEventListener("load", () => {
    const body = document.body,
          div = document.createElement('div');

    const template = `
      <div id="${settings.cssLabel}Back" class="clafoutis__overlay" onclick="clafoutis.closeToolBox();"></div>
      <div id="${settings.cssLabel}" class="clafoutis container">
        <div class="clafoutis__append__message" id="${settings.cssLabel}Messages">
          <div class="clafoutis__title">${settings.lang.msgTitle}</div>
          <div id="${settings.cssLabel}MessagesContent"></div>
        </div>
        <div id="${settings.cssLabel}Services" class="clafoutis__content">
          <div class="clafoutis__title">${settings.lang.mainTitle}</div>
          <p class="clafoutis__info">${settings.lang.disclaimer}</p>
          <div class="clafoutis__ask">
            <div class="clafoutis__ask__title">${settings.lang.askTitle}</div>
            <div class="btn-block">
              <div id="clafoutisAllAllowed" class="btn btn-clafoutis btn-clafoutis--allow  btn-clafoutis--allallow" onclick="clafoutis.allowAllServices();">
                ${settings.lang.allowAll}
              </div>
              <div id="clafoutisAllDenied" class="btn btn-clafoutis btn-clafoutis--deny btn-clafoutis--alldeny" onclick="clafoutis.denyAllServices();">
                ${settings.lang.denyAll}
              </div>
            </div>
          </div>
          <div class="${settings.cssLabel}ServicesListWrapper">
            <div id="${settings.cssLabel}ServicesList"></div>
          </div>
        </div>
        <div id="${settings.cssLabel}ClosePanel" class="clafoutis__close" onclick="clafoutis.closeToolBox();">&times;</div>
      </div>
    `;

    div.id = settings.cssLabel + 'RootBox';
    body.appendChild(div, body);
    div.innerHTML = template; 
  });  
}

/**
 * Display a message in tool box
 * 
 * @param  {string}  message  -  The given message
 * @param  {boolean} hideAll  -  Show only the given message
 * 
 * @return {none}
 */
function displayMessage(message, hideAll) {
  const template = `
    <div class="${settings.cssLabel}Line">
      ` + message + `
    </div>
  `;

  applyCss(settings.cssLabel + 'AllAllowed', 'display', 'none');
  applyCss(settings.cssLabel + 'AllDenied', 'display', 'none');

  if (hideAll) {
    applyCss(settings.cssLabel + 'Messages', 'display', 'block');
    applyCss(settings.cssLabel + 'Services', 'display', 'none');
  }

  document.getElementById(settings.cssLabel + 'MessagesContent').innerHTML = template;
}

/**
 * see displayMessage(message) comment
 * 
 * @param  {string}  message  -  The given message
 * @param  {boolean} hideAll  -  Show only the given message
 * 
 * @return {none}
 */
export function genericDisplayMessage(message, hideAll) {
  displayMessage(message, hideAll);
}

/**
 * Display services list in tool box content
 * 
 * @param  {Object}  service  -  The service configuration
 * 
 * @return {none}
 */
export function displayServicesInToolBox(service) {

  const template = `
    <div class="clafoutis__section">
      <div class="clafoutis__section__title">
        ${service.category}
      </div>
      <div class="clafoutis__section__details">
        ${service.description ? service.description : ''}
      </div>
      <div id="${service.code}Line" class="clafoutis__section__sub">
        <div class="btn-wrap">
          <strong>${service.uri ? `<a href="${service.uri}" target="_blank" rel="noopener">${service.title}</a>` : `${service.title}`}</strong>
          <div id="${service.code}Allowed" class="btn btn-clafoutis btn-clafoutis--allow" onclick="clafoutis.allowService(\'${service.code}\');">
            ${settings.lang.allow}
          </div>
          <div id="${service.code}Denied" class="btn btn-clafoutis btn-clafoutis--deny" onclick="clafoutis.denyService(\'${service.code}\');">
            ${settings.lang.deny}
          </div>
        </div>
      </div>
    </div>
  `;

  document.getElementById(settings.cssLabel + 'ServicesList').innerHTML += template;

  if (genericReadCookie()) {
    if (genericReadCookie().includes(service.code)) {
      addClassToId(service.code + 'Allowed', 'active');
    } else {
      addClassToId(service.code + 'Denied', 'active');
    }
  } else {
    addClassToId(settings.cssLabel + 'AllDenied', 'active');
    addClassToId(service.code + 'Denied', 'active');
  }
}

/**
 * Apply the given css style to a specific HTML element
 * 
 * @param  {string}  id  -  The HTML element id
 * @param  {string}  property  -  The CSS property
 * @param  {string}  value  -  The CSS value
 * 
 * @return {none}
 */
function applyCss(id, property, value) {
  if (document.getElementById(id) !== null) {
    document.getElementById(id).style[property] = value;
  }
}

/**
 * Add class
 * 
 */
export function addClassToId(id, className) {
  let el = document.getElementById(id);
  if (el !== null) {
    if (el.classList)
      el.classList.add(className);
    else
      el.className += ' ' + className;
  }
}
export function removeClassToId(id, className) {
  let el = document.getElementById(id);
  if (el !== null) {
    if (el.classList)
      el.classList.remove(className);
    else
      el.className = el.className.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');;
  }
}

/**
 * Open tool box
 * 
 * @return {none}
 */
export function openToolBox() {
  settings.adblocker ? displayMessage(settings.lang.useAdblocker, true) : null;

  applyCss(settings.cssLabel, 'display', 'block');
  // ClafoutisBack HTML component is used to close the pop-up tool 
  // automatically when user taps next to tool
  applyCss(settings.cssLabel + 'Back', 'display', 'block');
}

/**
 * Close tool box
 * 
 * @return {none}
 */
export function closeToolBox() {
  applyCss(settings.cssLabel, 'display', 'none');
  applyCss(settings.cssLabel + 'Back', 'display', 'none');
}

/**
 * Display cookie banner consent
 * 
 * @return {none}
 */
export function displayCookieBanner() {
  window.addEventListener("load", () => {
    const body = document.body,
          div = document.createElement('div'),
          appendChildId = settings.cookiesBanner.appendChildId,
          HTMLComponent = document.getElementById(appendChildId);

    const template = `
      <div class="cookies__bar">
        <div class="cookies__panel">
          <div class="container">
            <div class="cookies__content">
              <div class="cookies__message">
                <p>${settings.lang.cookieBannerMessage}</p>
              </div>
              <div class="cookies__btn-wrap">
                <a href="javascript:clafoutis.agreeCookiesConsent();" class="btn btn-cookies btn-cookies--close active">
                  ${settings.lang.cookieBannerAgreeButton}
                </a>
                <a href="javascript:clafoutis.openToolBox();" class="btn btn-cookies btn-cookies--open">
                  ${settings.lang.cookieBannerClafoutisButton}  
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    `;
  
    div.id = settings.cssLabel + 'RootBanner';

    appendChildId === 'body' ? body.appendChild(div, body)
                             : HTMLComponent.appendChild(div, HTMLComponent);

    div.innerHTML = template;
  });
}

/**
 * Set agreeCookiesConsent setting to true 
 * as a session variable to keep the choice 
 * and don't display the cookies consent banner
 * in the future
 * 
 * @return {none}
 */
export function agreeCookiesConsent() {
  localStorage.setItem("agreeCookiesConsent", true); 
  applyCss(settings.cssLabel + 'RootBanner', 'display', 'none');
}