'use strict';

var _settings = {
  "highPrivacy": false, // default value
  "cssLabel": "clafoutis", // default value
  "knownServices": [],
  "allServices": [],
  "adblocker": true,
  "cookiesBanner": {
  	"active": true, // default value
  	"appendChildId": "body" // default value
  },
  "lang" : {
    "mainTitle": __("mainTitle"),
    "disclaimer": __("disclaimer"),
    "askTitle": __("askTitle"),
    "allow": __("allow"),
    "allowAll": __("allowAll"),
    "deny": __("deny"),
    "denyAll": __("denyAll"),
    "noServices": __("noServices"),
    "useAdblocker": __("useAdblocker"),
    "cookieBannerMessage": __("cookieBannerMessage"),
    "cookieBannerAgreeButton": __("cookieBannerAgreeButton"),
    "cookieBannerClafoutisButton": __("cookieBannerClafoutisButton"),
    "msgTitle": __("msgTitle")
  }
}

module.exports = _settings


