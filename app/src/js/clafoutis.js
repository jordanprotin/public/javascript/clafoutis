'use strict';

import '../scss/app.scss';

import settings from './modules/_settings.js';
import { init, run } from './modules/_config.js';
import { openToolBox, closeToolBox, agreeCookiesConsent } from './modules/_interface.js';
import { 
  allowService, 
  denyAllServices, 
  allowAllServices, 
  denyService, 
  addCookieForSpecificService,
  deleteCookieForSpecificService } from './modules/_cookie.js';

var _clafoutis = {
  // store temporarily the wanted services. 
  // As a reminder, to inject services from GTM :
  // clafoutis.services.push({
  //  'code': 'analytics'
  // });
  'services': []
}

/**
 * Init global script
 * 
 * @param  {Object}  params  -  highPrivacy and/or cssLabel
 * @return {none}
 */
_clafoutis.init = function(params) {
  init(params);
}

/**
 * Dynamic addition of one or more cookies for a particular service
 *  
 * @param  {Object}  wishedCookies  -  key = serviceCode / value = an array which contains cookies name
 * @return {none}
 */
_clafoutis.cookies_push = function(wishedCookies) {
  // The load event is triggered when 
  // all resources are entirely loaded.
  window.addEventListener("load", (event) => {
    addCookieForSpecificService(wishedCookies, _clafoutis.services, null);
  }); 
}

/**
 * Shortcut to add Google Analytics UA cookie in analytics service
 * 
 * @param {string}  UA  -  The given Google Analytics UA value
 */
_clafoutis.cookies_push_UA = function(UA) {
  window.addEventListener("load", (event) => {
    addCookieForSpecificService(null, _clafoutis.services, UA);
  }); 
}

/**
 * Dynamic suppression of one or more cookies for a particular service
 * 
 * @param  {Object}  wishedCookies  -  key = serviceCode / value = an array which contains cookies name
 * @return {none}
 */
_clafoutis.cookies_splice = function(wishedCookies) {
  window.addEventListener("load", (event) => {
    deleteCookieForSpecificService(wishedCookies, _clafoutis.services);
  }); 
}

/**
 * Run system from GTM container (checks out Clafoutis tag)
 * 
 * @return {none} 
 */
_clafoutis.run = function() {
  settings.adblocker = false;
  window.addEventListener("load", (event) => {
    run(_clafoutis.services);
  });  
}

/**
 * Open tool box
 * 
 * @return {none}
 */
_clafoutis.openToolBox = function() {
  openToolBox();
}

/**
 * Agree cookies consent from the cookie banner
 * 
 * @return {none}
 */
_clafoutis.agreeCookiesConsent = function() {
  agreeCookiesConsent();
}

/**
 * Close tool box
 * 
 * @return {none}
 */
_clafoutis.closeToolBox = function() {
  closeToolBox();
}

/**
 * Allow a specific service
 * 
 * @param  {string  serviceCode  - The given service code (Ex: analytics)
 * @return {none}
 */
_clafoutis.allowService = function (serviceCode) {
  allowService(serviceCode);
}

/**
 * Deny all services
 * 
 * @return {none}
 */
_clafoutis.denyAllServices = function () {
  denyAllServices();
}

/**
 * Allow all services
 * 
 * @return {none}
 */
_clafoutis.allowAllServices = function () {
  allowAllServices();
}

/**
 * Deny a specific service
 * 
 * @param  {string  serviceCode  - The given service code (Ex: analytics)
 * @return {none}
 */
_clafoutis.denyService = function (serviceCode) {
  denyService(serviceCode);
}

module.exports = _clafoutis